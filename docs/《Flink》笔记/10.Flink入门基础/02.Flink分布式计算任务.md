---
title: Flink分布式计算任务
date: 2021-11-23 15:27:38
permalink: /pages/974b7f/
categories:
  - 《Flink》笔记
tags:
  - flink
---



Apache Flink is a framework and distributed processing engine for stateful computations over *unbounded and bounded* data streams

1. 有界的流和无界的流
2. 分布式的计算引擎
3. 有状态（这个就是重点，也是难点，当然也是flink比较有特色的功能点）

<!-- more -->

## 1 有状态的流和分布式计算

### 1.1 单词计数案例再次演示

```java 
/**
 * 单词计数
 * 没有设置并行度
 * 默认电脑有几个cpu core就会有几个并行度
 */
public class WordCount {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment
                .createLocalEnvironmentWithWebUI(new Configuration());

        DataStreamSource<String> dataStream = env.socketTextStream("localhost", 8888);

        SingleOutputStreamOperator<Tuple2<String, Long>> wordOneStream = dataStream.flatMap(new FlatMapFunction<String, Tuple2<String, Long>>() {
            @Override
            public void flatMap(String line, Collector<Tuple2<String, Long>> out) throws Exception {
                String[] fields = line.split(",");
                for (String word : fields) {
                    out.collect(Tuple2.of(word, 1L));
                }
            }
        });

        SingleOutputStreamOperator<Tuple2<String, Long>> result = wordOneStream
                .keyBy(0)
                .sum(1);

        result.print();

        env.execute(WordCount.class.getSimpleName());
    }
}
```

访问地址8081端口

```
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-runtime-web_2.11</artifactId>
    <version>${flink.version}</version>
    <!-- <scope>provided</scope>-->
</dependency>
```

![image-20191114232854448](https://gitee.com/kflys/uPic/raw/master/uPic/202111231534551.png)



![有状态的流](https://gitee.com/kflys/uPic/raw/master/uPic/202111231534684.png)

数据流

![](https://gitee.com/kflys/uPic/raw/master/uPic/202111231530480.png)

![并行度](https://gitee.com/kflys/uPic/raw/master/uPic/202111231535970.png)

修改一下并行度为4

![image-20191115001027412](https://gitee.com/kflys/uPic/raw/master/uPic/202111231534908.png)



![设置并行度](D:\document\flink\20200207_flink2_课件\20200207_flink2_课件\深入浅出Flink之task.assets\设置并行度.png)

### 1.2  并行度Slot、Task

​		Flink的每个TaskManager为集群提供solt。 solt的数量通常与每个TaskManager节点的可用CPU内核数成比例。一般情况下你的slot数是你每个节点的cpu的核数。

![1574472572814](D:\document\flink\20200207_flink2_课件\20200207_flink2_课件\assets\1574472572814.png)

### 1.3  并行度

​		一个Flink程序由多个任务组成(source、transformation和 sink)。 一个任务由多个并行的实例(线程)来执行， 一个任务的并行实例(线程)数目就被称为该任务的并行度。

### 1.4  并行度的设置

一个任务的并行度设置可以从多个层次指定

- Operator Level（算子层次）

- Execution Environment Level（执行环境层次）

- Client Level（客户端层次）

- System Level（系统层次）

#### 1.4.1 算子层次

![1574472860477](https://gitee.com/kflys/uPic/raw/master/uPic/202111231537638.png)

#### 1.4.2 执行环境层次

![1574472880358](https://gitee.com/kflys/uPic/raw/master/uPic/202111231537475.png)

#### 1.4.3 客户端层次

​		并行度可以在客户端将job提交到Flink时设定，对于CLI客户端，可以通过-p参数指定并行度

~~~
./bin/flink run -p 10 WordCount.jar
~~~

#### 1.4.4 系统层次

​	在系统级可以通过设置flink-conf.yaml文件中的parallelism.default属性来指定所有执行环境的默认并行度

## 2  任务提交（集群模式）

| 模式             | 提交语句                                                     |
| ---------------- | ------------------------------------------------------------ |
| StandAlnoe模式   | flink run -c streaming.slot.lesson01.WordCount -p 2 flinklesson-1.0-SNAPSHOT.jar |
| Flink on Yarn(1) | flink run -m yarn-cluster -p 2 -yn 2 -yjm 1024 -ytm 1024 -c streaming.slot.lesson01.WordCount flinklesson-1.0-SNAPSHOT.jar |
| Flink on Yarn(2) | yarn-session.sh -n 2 -jm 1024 -tm 1024<br/>flink run ./examples/batch/WordCount.jar -input hdfs://hadoop100:9000/LICENSE -output hdfs://hadoop100:9000/wordcount-result.txt |

standalone集群：

jobmanager：

​		192.168.167.251

TaskManager

​		192.168.167.251

​		192.168.167.252

### 2.1  把任务提交到yarn上

~~~
# 演示一：
flink run -m yarn-cluster -p 2 -yn 2 -yjm 1024 -ytm 1024 -c streaming.slot.lesson01.WordCount flinklesson-1.0-SNAPSHOT.jar

# 演示二：
flink run -m yarn-cluster -p 3 -yn 2 -yjm 1024 -ytm 1024 -c streaming.slot.lesson01.WordCount flinklesson-1.0-SNAPSHOT.jar

~~~

### 2.2  把任务提交到standalone集群

![1574475633598](https://gitee.com/kflys/uPic/raw/master/uPic/202111231538142.png)



~~~
演示一：
flink run -c streaming.slot.lesson01.WordCount -p 2 flinklesson-1.0-SNAPSHOT.jar
演示二：
flink run -c streaming.slot.lesson01.WordCount -p 3 flinklesson-1.0-SNAPSHOT.jar
~~~

## 3. 任务task

### 3.1  数据传输的方式

#### 3.1.1 forward strategy

![1574478499283](https://gitee.com/kflys/uPic/raw/master/uPic/202111231530297.png)

1. 一个 task 的输出只发送给一个 task 作为输入
2. 如果两个 task 都在一个 JVM 中的话，那么就可以避免网络开销

#### 3.1.2 key based strategy (key by)

![1574478613578](https://gitee.com/kflys/uPic/raw/master/uPic/202111231530565.png)

1. 数据需要按照某个属性(我们称为 key)进行分组(或者说分区)
2. 相同 key 的数据需要传输给同一个 task，在一个 task 中进行处理

#### 3.2.3 broadcast strategy

![1574478760221](https://gitee.com/kflys/uPic/raw/master/uPic/202111231540797.png)

#### 3.2.4 random strategy

![1574478869374](https://gitee.com/kflys/uPic/raw/master/uPic/202111231540747.png)



1. 数据随机的从一个 task 中传输给下一个 operator 所有的 subtask
2. 保证数据能均匀的传输给所有的 subtask

### 3.2  Operator Chain

~~~
public class WordCount {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        String topic="testSlot";
        Properties consumerProperties = new Properties();
        consumerProperties.setProperty("bootstrap.servers","192.168.167.254:9092");
        consumerProperties.setProperty("group.id","testSlot_consumer");


        FlinkKafkaConsumer011<String> myConsumer =
                new FlinkKafkaConsumer011<>(topic, new SimpleStringSchema(), consumerProperties);

        DataStreamSource<String> data = env.addSource(myConsumer).setParallelism(3);

        SingleOutputStreamOperator<Tuple2<String, Integer>> wordOneStream = data.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public void flatMap(String line,
                                Collector<Tuple2<String, Integer>> out) throws Exception {
                String[] fields = line.split(",");
                for (String word : fields) {
                    out.collect(Tuple2.of(word, 1));
                }
            }
        }).setParallelism(2);

        SingleOutputStreamOperator<Tuple2<String, Integer>> result = wordOneStream.keyBy(0).sum(1).setParallelism(2);

        result.map( tuple -> tuple.toString()).setParallelism(2)
                .print().setParallelism(1);

        env.execute("WordCount2");

    }
}
~~~

![1574479305528](https://gitee.com/kflys/uPic/raw/master/uPic/202111231530944.png)****

![1574479086824](https://gitee.com/kflys/uPic/raw/master/uPic/202111231540398.png)



![1574479147372](https://gitee.com/kflys/uPic/raw/master/uPic/202111231540572.png)



#### 3.2.1 Operator Chain的条件：

1. 数据传输策略是 forward strategy
2. 在同一个 TaskManager 中运行

并行度设置为1：

![1574480303336](https://gitee.com/kflys/uPic/raw/master/uPic/202111231530093.png)

![1574480321456](https://gitee.com/kflys/uPic/raw/master/uPic/202111231530489.png)

![image-20211123154148772](https://gitee.com/kflys/uPic/raw/master/uPic/202111231541818.png)
