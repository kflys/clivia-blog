---
title: Kafka环境搭建
date: 2021-09-29 14:06:47
permalink: /pages/3fec6c/
categories:
  - 《大数据环境搭建》笔记
tags:
  - kafka
---
## kafka环境搭建

### 下载软件

<br />[_点击下载 https://archive.apache.org/dist/kafka/1.0.1/kafka_2.11-1.0.1.tgz_](https://archive.apache.org/dist/kafka/1.0.1/kafka_2.11-1.0.1.tgz)<br />

### 修改配置文件

- vi server.properties



```shell
#指定kafka对应的broker id ，唯一(eg：node01 0,node02 1,node03 2)
broker.id=0
#指定数据存放的目录
log.dirs=/kfly/install/kafka/kafka-logs
#指定zk地址
zookeeper.connect=node01:2181,node02:2181,node03:2181
#指定是否可以删除topic ,默认是false 表示不可以删除
delete.topic.enable=true
#指定broker主机名,node01 node02 node03
host.name=node01
```


- 修改kafka环境变量



```shell
export KAFKA_HOME=/kfly/install/kafka
export PATH=$PATH:$KAFKA_HOME/bin
```


- 分发到其他节点，然后运行



```shell
#!/bin/sh
case $1 in 
"start"){
for host in node01 node02 node03 
do
  ssh $host "source /etc/profile; nohup /kfly/install/kafka/bin/kafka-server-start.sh /kfly/install/kafka/config/server.properties > /dev/null 2>&1 &"   
  echo "$host kafka is running..."  
done
};;

"stop"){
for host in node01 node02 node03 
do
  ssh $host "source /etc/profile; nohup /kfly/install/kafka/bin/kafka-server-stop.sh >/dev/null  2>&1 &"   
  echo "$host kafka is stopping..."  
done
};;
esac
```


### kafka监控工具的安装


#### Kafka Manager


```shell
kafkaManager它是由雅虎开源的可以监控整个kafka集群相关信息的一个工具。
（1）可以管理几个不同的集群
（2）监控集群的状态(topics, brokers, 副本分布, 分区分布)
（3）创建topic、修改topic相关配置
```


- [点击下载 https://github.com/yahoo/kafka-manager/releases](https://github.com/yahoo/kafka-manager/releases)
- vim application.conf
```shell
#修改kafka-manager.zkhosts的值，指定kafka集群地址
kafka-manager.zkhosts="node01:2181,node02:2181,node03:2181"
```

- 4、启动kafka-manager
   - 启动zk集群，kafka集群，再使用root用户启动kafka-manager服务。
   - bin/kafka-manager 默认的端口是9000，可通过 -Dhttp.port，指定端口
   - -Dconfig.file=conf/application.conf指定配置文件
```shell
nohup bin/kafka-manager -Dconfig.file=conf/application.conf -Dhttp.port=8080 &
```

- KafkaOffsetMonitor



```shell
该监控是基于一个jar包的形式运行，部署较为方便。只有监控功能，使用起来也较为安全

(1)消费者组列表
(2)查看topic的历史消费信息.
(3)每个topic的所有parition列表(topic,pid,offset,logSize,lag,owner)
(4)对consumer消费情况进行监控,并能列出每个consumer offset,滞后数据。
```


- 1、下载安装包 [下载](https://github.com/quantifind/KafkaOffsetMonitor/tags)
```shell
KafkaOffsetMonitor-assembly-0.2.0.jar
```

- 2、在服务器上新建一个目录kafka_moitor，把jar包上传到该目录中
- 3、在kafka_moitor目录下新建一个脚本
   - vim start_kafka_web.sh
```shell
#!/bin/sh
java -cp KafkaOffsetMonitor-assembly-0.2.0.jar com.quantifind.kafka.offsetapp.OffsetGetterWeb --zk node01:2181,node02:2181,node03:2181 --port 8089 --refresh 10.seconds --retain 1.days
```

- 4、启动脚本
```shell
nohup sh start_kafka_web.sh &
```


<a name="8f9aa47d"></a>
#### 10.3.2 Kafka Eagle


- 1、下载Kafka Eagle安装包
   - [http://download.smartloli.org/](http://download.smartloli.org/)
      - kafka-eagle-bin-1.2.3.tar.gz
- 2、解压
   - tar -zxvf kafka-eagle-bin-1.2.3.tar.gz -C /kkb/install
   - 解压之后进入到kafka-eagle-bin-1.2.3目录中
      - 得到kafka-eagle-web-1.2.3-bin.tar.gz
      - 然后解压  tar -zxvf kafka-eagle-web-1.2.3-bin.tar.gz
      - 重命名  mv kafka-eagle-web-1.2.3  kafka-eagle-web
- 3、修改配置文件
   - 进入到conf目录
      - 修改system-config.properties



```shell
# 填上你的kafka集群信息
kafka.eagle.zk.cluster.alias=cluster1
cluster1.zk.list=node01:2181,node02:2181,node03:2181

# kafka eagle页面访问端口
kafka.eagle.webui.port=8048

# kafka sasl authenticate
kafka.eagle.sasl.enable=false
kafka.eagle.sasl.protocol=SASL_PLAINTEXT
kafka.eagle.sasl.mechanism=PLAIN
kafka.eagle.sasl.client=/kfly/install/kafka-eagle-bin-1.2.3/kafka-eagle-web/conf/kafka_client_jaas.conf

#  添加刚刚导入的ke数据库配置，我这里使用的是mysql
kafka.eagle.driver=com.mysql.jdbc.Driver
kafka.eagle.url=jdbc:mysql://node02:3306/ke?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull
kafka.eagle.username=root
kafka.eagle.password=123456
```


- 4、配置环境变量
- vi /etc/profile



```shell
    export KE_HOME=/kfly/install/kafka-eagle-web
    export PATH=$PATH:$KE_HOME/bin
```


- 5、启动kafka-eagle /访问



```shell
  sh bin/ke.sh start
  # 访问 `http://node01:8048/ke`
  # 用户名：admin password：123456
```
