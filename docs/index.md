---
home: true
# heroImage: /img/web.png
heroText: clivia's blog
tagline: 君子谦谦,温和有礼,有才而不骄,得志而不傲,居于谷而不卑。
# actionText: 立刻进入 →
# actionLink: /web/
# bannerBg: auto # auto => 网格纹背景(有bodyBgImg时无背景)，默认 | none => 无 | '大图地址' | background: 自定义背景样式       提示：如发现文本颜色不适应你的背景时可以到palette.styl修改$bannerTextColor变量

features: # 可选的
  - title: 大数据
    details: hadoop、spark、hive、hbase等技术
    link: /bigdata/ # 可选
    imgUrl: /img/web.png # 可选
  - title: 技术
    details: 技术文档、教程、技巧、总结等文章
    link: /technology/
    imgUrl: /img/other.png
  - title: 项目
    details: Java后端、数据库开发
    link: /project/
    imgUrl: /img/ui.png


# 文章列表显示方式: detailed 默认，显示详细版文章列表（包括作者、分类、标签、摘要、分页等）| simple => 显示简约版文章列表（仅标题和日期）| none 不显示文章列表
# postList: detailed
# simplePostListLength: 10 # 简约版文章列表显示的文章数量，默认10。（仅在postList设置为simple时生效）
---


<!-- 小熊猫 -->
<!-- <img src="/img/panda-waving.png" class="panda no-zoom" style="width: 130px;height: 115px;opacity: 0.8;margin-bottom: -4px;padding-bottom:0;position: fixed;bottom: 0;left: 0.5rem;z-index: 1;"> -->
