// nav
module.exports = [
  {
	text: '首页', 
	link: '/' ,
  },
  {
    text: '大数据',
    link: '/bigdata/', 
  },
  {
    text: '技术',
    link: '/technology/',
  },
  {
    text: '项目',
    link: '/project/',
  },
  {
    text: '更多',
    link: '/more/',
  },
  { 
	text: '关于', 
	link: '/about/' ,
  },
  {
    text: '收藏',
    link: '/pages/beb6c0bd8a66cea6/',
  },
  {
    text: '索引',
    link: '/archives/',
    items: [
      { text: '分类', link: '/categories/' },
      { text: '标签', link: '/tags/' },
      { text: '归档', link: '/archives/' },
    ],
  },
]
