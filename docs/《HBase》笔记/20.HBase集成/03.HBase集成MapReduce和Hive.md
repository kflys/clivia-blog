---
title: HBase集成MapReduce和Hive
date: 2021-09-30 10:54:42
permalink: /pages/9f41c3/
categories:
  - 《HBase》笔记
tags:
  - hbase
  - mapreduce
  - hive
---

---

## HBase集成MapReduce


- HBase表中的数据最终都是存储在HDFS上，HBase天生的支持MR的操作，我们可以通过MR直接处理HBase表中的数据，并且MR可以将处理后的结果直接存储到HBase表中。
   - 参考地址：<[http://hbase.apache.org/book.html#mapreduce](http://hbase.apache.org/book.html#mapreduce)
- 需求：读取HBase当中myuser这张表的数据，将数据写入到另外一张myuser2表里面去



```
 <dependency>
   <groupId>org.apache.hadoop</groupId>
   <artifactId>hadoop-client</artifactId>
   <version>2.6.0-mr1-cdh5.14.2</version>
</dependency>
<dependency>
  <groupId>org.apache.hbase</groupId>
  <artifactId>hbase-client</artifactId>
  <version>1.2.0-cdh5.14.2</version>
</dependency>
<dependency>
  <groupId>org.apache.hbase</groupId>
  <artifactId>hbase-server</artifactId>
  <version>1.2.0-cdh5.14.2</version>
</dependency>
```


```
class HBaseMapper extends TableMapper<Text,Put>{
  protected void map(ImmutableBytesWritable key, Result value, Context context){
    // key get rowKey
    // result -> put
    context.write(new Text(rowkey),put);
  }
}
class HbaseReducer extends TableReducer<Text,Put,ImmutableBytesWritable>{
  protected void reduce(Text key, Iterable<Put> values, Context context){
    for (Put put : values) {
      context.write(null,put);
    }
  }
}
public static void main(String[] args){
        Configuration conf = new Configuration();
        Scan scan = new Scan();
        Job job = Job.getInstance(conf);
        job.setJarByClass(HBaseMR.class);
        //使用TableMapReduceUtil 工具类来初始化我们的mapper
      TableMapReduceUtil.initTableMapperJob(
                                            TableName.valueOf(args[0]),
                                            scan,
                                            HBaseMapper.class,
                                            Text.class,
                                            Put.class,
                                            job
      																		);
        //使用TableMapReduceUtil 工具类来初始化我们的reducer
        TableMapReduceUtil.initTableReducerJob(
          															args[1],
          															HbaseReducer.class,
          															job);
        //设置reduce task个数
        job.setNumReduceTasks(1);
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
```


```shell
# 打成jar包提交到集群中运行
hadoop jar hbase_java_api-1.0-SNAPSHOT.jar com.kfly.HBaseMR t1 t2
```


- 需求
   - 通过bulkload的方式批量加载数据到HBase表中
   - 将我们hdfs上面的这个路径/hbase/input/user.txt的数据文件，转换成HFile格式，然后load到myuser2这张表里面去
- 知识点描述
   - 加载数据到HBase当中去的方式多种多样，我们可以使用HBase的javaAPI或者使用sqoop将我们的数据写入或者导入到HBase当中去，但是这些方式不是慢就是在导入的过程的占用Region资源导致效率低下
   - 我们也可以通过MR的程序，将我们的数据直接转换成HBase的最终存储格式HFile，然后直接load数据到HBase当中去即可
- HBase数据正常写流程回顾
![](http://kflys.gitee.io/upic/2020/04/01/uPic/hbase/assets/hbase-write.png#height=404&id=zSDVJ&originHeight=404&originWidth=1007&originalType=binary&ratio=1&status=done&style=none&width=1007)
- bulkload方式的处理示意图



![](http://kflys.gitee.io/upic/2020/04/01/uPic/hbase/assets/bulkload.png#height=671&id=Zxcrg&originHeight=671&originWidth=1627&originalType=binary&ratio=1&status=done&style=none&width=1627)


- 好处
   - 导入过程不占用Region资源
   - 能快速导入海量的数据
   - 节省内存
- 1、开发生成HFile文件的代码



```
// 1. map阶段
context.write(new ImmutableBytesWritable(Bytes.toBytes(split[0])),put);

// 2. main job
Configuration conf = HBaseConfiguration.create();
Connection connection = ConnectionFactory.createConnection(conf);
job.setMapOutputKeyClass(ImmutableBytesWritable.class);
job.setMapOutputValueClass(Put.class);
//指定输出的类型HFileOutputFormat2
job.setOutputFormatClass(HFileOutputFormat2.class);
HFileOutputFormat2
  .configureIncrementalLoad(
                            job,
                            table,                                       		   	 
                            conn.getRegionLocator(TableName.valueOf("t4"))
															);
// 3. 加载数据使用java Api
Table table = connection.getTable(tableName);
//构建LoadIncrementalHFiles加载HFile文件
LoadIncrementalHFiles load = new LoadIncrementalHFiles(configuration);
load.doBulkLoad(new Path("hdfs://node01:8020/hbase/output_file");
                
// 加载数据命令加载,先将hbase的jar包添加到hadoop的classpath路径下
export HBASE_HOME=/kfly/install/hbase-1.2.0-cdh5.14.2/
export HADOOP_HOME=/kfly/install/hadoop-2.6.0/
export HADOOP_CLASSPATH=`${HBASE_HOME}/bin/hbase mapredcp`
                
yarn jar /kfly/install/hbase-1.2.0-cdh5.14.2/lib/hbase-server-1.2.0-cdh5.14.2.jar   completebulkload /hbase/output_hfile myuser2
```


```shell
hadoop jar hbase_java_api-1.0-SNAPSHOT.jar com.kaikeba.HBaseLoad
```


## HBase集成Hive


- Hive提供了与HBase的集成，使得能够在HBase表上使用hive sql 语句进行查询、插入操作以及进行Join和Union等复杂查询，同时也可以将hive表中的数据映射到Hbase中



### 对比


- Hive
   - 数据仓库
      - 	Hive的本质其实就相当于将HDFS中已经存储的文件在Mysql中做了一个双射关系，以方便使用HQL去管理查询。
   - 用于数据分析、清洗
      - 	Hive适用于离线的数据分析和清洗，延迟较高
   - 基于HDFS、MapReduce
      - 	Hive存储的数据依旧在DataNode上，编写的HQL语句终将是转换为MapReduce代码执行。（不要钻不需要执行MapReduce代码的情况的牛角尖）
- HBase
   - 数据库
      - 是一种面向列存储的非关系型数据库。
   - 用于存储结构化和非结构话的数据
      - 适用于单表非关系型数据的存储，不适合做关联查询，类似JOIN等操作。
   - 基于HDFS
      - 数据持久化存储的体现形式是Hfile，存放于DataNode中，被ResionServer以region的形式进行管理。
   - 延迟较低，接入在线业务使用
      - 面对大量的企业数据，HBase可以直线单表大量数据的存储，同时提供了高效的数据访问速度。
- Hive和Hbase是两种基于Hadoop的不同技术，Hive是一种类SQL的引擎，并且运行MapReduce任务，Hbase是一种在Hadoop之上的NoSQL 的Key/vale数据库。这两种工具是可以同时使用的。就像用Google来搜索，用FaceBook进行社交一样，Hive可以用来进行统计查询，HBase可以用来进行实时查询，数据也可以从Hive写到HBase，或者从HBase写回Hive。



#### 拷贝jar包


- 将我们HBase的五个jar包拷贝到hive的lib目录下
- hbase的jar包都在/kfly/install/hbase-1.2.0-cdh5.14.2/lib
- 我们需要拷贝五个jar包名字如下



```
hbase-client-1.2.0-cdh5.14.2.jar                  
hbase-hadoop2-compat-1.2.0-cdh5.14.2.jar 
hbase-hadoop-compat-1.2.0-cdh5.14.2.jar  
hbase-it-1.2.0-cdh5.14.2.jar    
hbase-server-1.2.0-cdh5.14.2.jar
```


- 我们直接在node03执行以下命令，通过创建软连接的方式来进行jar包的依赖



```shell
ln -s /kfly/install/hbase-1.2.0-cdh5.14.2/lib/hbase-client-1.2.0-cdh5.14.2.jar              /kfly/install/hive-1.1.0-cdh5.14.2/lib/hbase-client-1.2.0-cdh5.14.2.jar
```


#### 修改hive-site.xml


- 添加以下两个属性的配置



```
<property>
		<name>hive.zookeeper.quorum</name>
		<value>node01,node02,node03</value>
</property>
 <property>
		<name>hbase.zookeeper.quorum</name>
		<value>node01,node02,node03</value>
</property>
```


#### 修改hive-env.sh


```
export HADOOP_HOME=/export/servers/hadoop-2.6.0
export HBASE_HOME=/export/servers/hbase-1.2.0-cdh5.14.2
export HIVE_CONF_DIR=/export/servers/hive-1.1.0-cdh5.14.2/conf
```


### hive建表同步到hbase


```
-- hive当中建表
create external table if not exists course.score(id int,cname string,score int) row format delimited fields terminated by '\t' stored as textfile ;
-- 加载数据到hive
load data local inpath '/kfly/doc/hive-hbase.txt' into table score;
-- 创建hive管理表与HBase映射
create table course.hbase_score(id int,cname string,score int) stored by 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'  with serdeproperties("hbase.columns.mapping" = "cf:name,cf:score") tblproperties("hbase.table.name" = "hbase_score");
```


### hive外部表映射HBase表模型


```sql
-- 创建一张hbase表
create 'hbase_hive_score',{ NAME =>'cf'}
-- 建立hive的外部表，映射HBase当中的表以及字段
CREATE external TABLE course.hbase2hive(id int, name string, score int) STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler' WITH SERDEPROPERTIES ("hbase.columns.mapping" = ":key,cf:name,cf:score") TBLPROPERTIES("hbase.table.name" ="hbase_hive_score");
```
